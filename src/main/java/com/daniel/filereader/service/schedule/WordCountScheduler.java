package com.daniel.filereader.service.schedule;

import com.daniel.filereader.service.file.FileParser;
import com.daniel.filereader.service.print.AwesomePrinter;
import com.daniel.filereader.service.word.WordCountCache;
import com.daniel.filereader.service.word.WordCounter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class WordCountScheduler {

    private final static Logger LOGGER = LoggerFactory.getLogger(WordCountScheduler.class);

    @Value("${inputFiles.path}")
    private String filePath;

    @Autowired
    private AwesomePrinter printer;

    @Autowired
    private FileParser fileParser;

    @Autowired
    private WordCounter wordCounter;

    @Autowired
    private WordCountCache wordCountCache;

    @Scheduled(fixedRate = 5000, initialDelay = 0)
    public void scheduler() {
        List<String> filePaths = fileParser.retrieveAllFilePathsFromPathAsString(filePath);

        CompletableFuture[] completableFutures = filePaths.stream()
                .map(filePath -> CompletableFuture.runAsync(() -> wordCounter.readFileAndCountWords(filePath)))
                .toArray(CompletableFuture[]::new);

        CompletableFuture.allOf(completableFutures).join();

        CompletableFuture<Void> printFuture = CompletableFuture.runAsync(() -> {
            printer.printCount(filePaths);
            wordCountCache.resetCache();
        });

        runFuture(printFuture);
    }

    private void runFuture(Future future) {
        try {
            future.get();
        } catch (InterruptedException | ExecutionException exception) {
            LOGGER.error(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

}
