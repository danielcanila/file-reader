package com.daniel.filereader.service.print;

import com.daniel.filereader.service.word.WordCount;
import com.daniel.filereader.service.word.WordCountCache;
import com.daniel.filereader.service.word.WordCounter;
import org.apache.tomcat.util.buf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AwesomePrinter {

    private Logger PRINTER = LoggerFactory.getLogger(AwesomePrinter.class);

    private static final String OUTPUT_SEPARATOR = "-------------------------------------------";
    private static final String SEPARATOR = " ";
    private static final String EQUALS = " = ";
    private static final String PLUS = " + ";

    @Autowired
    private WordCountCache wordCountCache;

    /**
     * This method will read the cached word count and will print it ordered by totalOccurrences starting from the biggest value.
     * Output example : <word> <total occurrences> = <occurrences in file1> + <occurrences in file2> + .... <occurrences in fileN>
     *
     * @param
     */
    public void printCount(List<String> filePaths) {
        List<WordCount> wordCounts = wordCountCache.getWordCounts();

        PRINTER.info(OUTPUT_SEPARATOR);
        wordCounts.stream()
                .sorted(Comparator.comparing(WordCount::getTotalOccurrences).reversed())
                .map(wordCount -> buildPrintOutput(wordCount, filePaths))
                .forEach(output -> PRINTER.info(output));
        PRINTER.info(OUTPUT_SEPARATOR);
    }

    private String buildPrintOutput(WordCount wordCount, List<String> filePaths) {
        String splitSum = filePaths.stream()
                .map(s -> wordCount.getCountsForPath(s).toString())
                .collect(Collectors.joining(PLUS));

        return new StringBuffer()
                .append(wordCount.getWord())
                .append(SEPARATOR)
                .append(wordCount.getTotalOccurrences())
                .append(EQUALS)
                .append(splitSum)
                .toString();
    }
}
