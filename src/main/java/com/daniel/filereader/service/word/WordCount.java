package com.daniel.filereader.service.word;


import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class WordCount {

    private final String word;
    private final Map<String, Long> wordCountPerFile = new ConcurrentHashMap<>();

    WordCount(String word, String path, Long countNumber) {
        this.word = word;
        wordCountPerFile.merge(path, countNumber, (existingValue, newValue) -> existingValue + newValue);
    }

    public String getWord() {
        return word;
    }

    public Long getCountsForPath(String path) {
        return Optional.ofNullable(wordCountPerFile.get(path)).orElse(0L);
    }

    public Long getTotalOccurrences() {
        return wordCountPerFile.entrySet()
                .stream()
                .mapToLong(Map.Entry::getValue)
                .sum();
    }

    WordCount incrementCountBy(String path, Long countNumber) {
        wordCountPerFile.merge(path, countNumber, (existingValue, newValue) -> existingValue + newValue);
        return this;
    }
}
