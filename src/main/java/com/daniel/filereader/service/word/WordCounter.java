package com.daniel.filereader.service.word;

import com.daniel.filereader.service.file.FileParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class WordCounter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordCounter.class);

    @Autowired
    private FileParser fileParser;

    @Autowired
    private WordCountCache wordCountCache;

    public void readFileAndCountWords(String path) {
        LOGGER.info("Processing for path {} started on thread {}", path, Thread.currentThread());

        String fileContent = fileParser.readFileContentAsString(path);
        List<String> words = fileParser.splitTextInWords(fileContent);

        words.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .forEach((word, countNumber) -> wordCountCache.addWordToCount(word, path, countNumber));
    }

}
