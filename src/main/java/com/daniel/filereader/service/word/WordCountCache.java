package com.daniel.filereader.service.word;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WordCountCache {

    private final Map<String, WordCount> wordCounts = new ConcurrentHashMap<>();

    void addWordToCount(String word, String path, Long countNumber) {
        wordCounts.merge(word, new WordCount(word, path, countNumber), (existingValue, newValue) -> existingValue.incrementCountBy(path, countNumber));
    }

    public List<WordCount> getWordCounts() {
        return new ArrayList<>(wordCounts.values());
    }

    public void resetCache() {
        wordCounts.clear();
    }
}
