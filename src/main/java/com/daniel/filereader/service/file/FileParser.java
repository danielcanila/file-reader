package com.daniel.filereader.service.file;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileParser {

    private static final String SEPARATOR = "\\W+";
    private static final String DEFAULT_CHARSET = "UTF-8";

    /**
     * This method will split a sentence in words separating them by any non-character char.
     *
     * @param text
     * @return a List of words
     */
    public List<String> splitTextInWords(String text) {
        return Arrays.asList(text.split(SEPARATOR));
    }

    /**
     * This method will read by path, and return its content as a String.
     *
     * @param path
     * @return the content of the file as String.
     */
    public String readFileContentAsString(String path) {
        try {
            byte[] inputFileOnes = Files.readAllBytes(Paths.get(path));
            return new String(inputFileOnes, Charset.forName(DEFAULT_CHARSET));
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * This method will read and return the content of all the files included in the given path.
     * If the path is a file, it will return a list with a single element.
     *
     * @param path
     * @return list of files content
     */
    public List<String> retrieveAllFilePathsFromPathAsString(String path) {
        return retrieveFilePath(new File(path));
    }

    private List<String> retrieveFilePath(File file) {
        if (file.isFile()) {
            return Arrays.asList(file.getPath());
        } else {
            return Arrays.stream(file.listFiles())
                    .flatMap(currentFile -> retrieveFilePath(currentFile).stream())
                    .collect(Collectors.toList());
        }

    }
}
