package com.daniel.filereader.service.file

import spock.lang.Specification

class FileParserUTSpec extends Specification {

    def fileParser = new FileParser()

    def "should returned a list of words for a random given sentence"() {
        given: "a random Lorem ipsum sentance"
        def sentence = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

        when: "splitting the sentence in words"
        def words = fileParser.splitTextInWords(sentence)

        then: "the result should contain 8 words"
        words.size() == 8
    }
}
