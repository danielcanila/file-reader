package com.daniel.filereader.service.print

import com.daniel.filereader.service.word.WordCount
import spock.lang.Specification

class AwesomePrinterUTSpec extends Specification {

    def printer = new AwesomePrinter()

    def "test build print output"() {
        given: "two paths"
        def pathOne = "filePathOne"
        def pathTwo = "filePathTwo"

        and: "a word count that is found in two files"
        def wordCount = new WordCount("testWord", pathOne, 4L)
        wordCount.incrementCountBy(pathTwo, 10L)

        when: "building the output"
        def output = printer.buildPrintOutput(wordCount, [pathOne, pathTwo] as List)

        then: "the output should be the desired one"
        output == "testWord 14 = 4 + 10"
    }
}
