package com.daniel.filereader.service.word

import spock.lang.Specification

class WordCountUTSpec extends Specification {

    public static final String TEST_PATH = "testPath"
    public static final String ANOTHER_TEST_PATH = "anotherTestPath"

    def "word count increment adds to the correct path"() {
        given: "a word count with the starting value of 10 for testPath"
        def wordCount = new WordCount("testWord", TEST_PATH, 10)

        and: "incrementing for the same path with 5"
        wordCount.incrementCountBy(TEST_PATH, 5)

        and: "incrementing for another path with 5"
        wordCount.incrementCountBy(ANOTHER_TEST_PATH, 5)

        when: "checking the count for testPath"
        def result = wordCount.getCountsForPath(TEST_PATH)

        then: "the result should be 15"
        result == 15
    }

    def "word count total occurrences works as expected"() {
        given: "a word count with the starting value of 10 for testPath"
        def wordCount = new WordCount("testWord", TEST_PATH, 10)

        and: "incrementing for the same path with 5"
        wordCount.incrementCountBy(TEST_PATH, 5)

        and: "incrementing for another path with 5"
        wordCount.incrementCountBy(ANOTHER_TEST_PATH, 5)

        when: "checking the count for testPath"
        def result = wordCount.getTotalOccurrences()

        then: "the result should be 15"
        result == 20
    }

}
