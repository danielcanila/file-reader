package com.daniel.filereader.service.word

import com.daniel.filereader.service.file.FileParser
import spock.lang.Specification

class WordCounterUTSpec extends Specification {


    FileParser fileParser = Stub(FileParser.class)
    WordCountCache wordCountCache = Mock(WordCountCache.class)
    def wordCounter = new WordCounter(fileParser: fileParser, wordCountCache: wordCountCache)

    def "path should be processed and added to global cache"() {
        given:
        def path = "testPath"
        def listOfWords = ["firstWord", "secondWord"] as List

        and:
        fileParser.readFileContentAsString(path) >> path
        fileParser.splitTextInWords(path) >> listOfWords

        when:
        wordCounter.readFileAndCountWords(path)

        then:
        2 * wordCountCache.addWordToCount(*_)

    }
}
