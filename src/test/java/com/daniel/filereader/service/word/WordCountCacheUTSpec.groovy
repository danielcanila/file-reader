package com.daniel.filereader.service.word

import spock.lang.Specification

class WordCountCacheUTSpec extends Specification {


    def "add word to count works as expected"() {

        given: "a word count cache"
        def wordCountCache = new WordCountCache()

        when: "adding two word counts with different paths"
        wordCountCache.addWordToCount("testWord", "testPathOne", 10)
        wordCountCache.addWordToCount("testWord", "testPathTwo", 5)

        then: "we should only have only one wordCount with 15 occurrences"
        wordCountCache.getWordCounts().size() == 1
        wordCountCache.getWordCounts()[0].getTotalOccurrences() == 15
    }
}
